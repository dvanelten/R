#to execute: source("/path/to/file/<filename>") or copy into R-Studio

pdf("/Users/dennisvanelten/Desktop/R-Studium/Dennis-R/Iris/Iris_ExplorationPlots.pdf")

iris <- read.csv("/Users/dennisvanelten/Desktop/R-Studium/Dennis-R/Iris/IRIS.csv")

###### simple exploration #####
head(iris,5)
#tail(iris,5)
dim(iris)
attributes(iris)
summary(iris)

#quantile(iris$sepal_length)
quantile(iris$sepal_length, c(0.1, 0.3, 0.65))   #specific quantiles


##### plots #####

hist(iris$sepal_length)
plot(density(iris$sepal_length))

table(iris$species)
pie(table(iris$species))
barplot(table(iris$species))


##### multiple variables #####

cov(iris$sepal_length, iris$petal_length)
cov(iris[,1:4]) #covariance between the entries (except "species")

cor(iris$sepal_length, iris$petal_length)
cor(iris[,1:4])

aggr <- aggregate(sepal_length ~ species, summary, data=iris)   #Splits data into subsets, computes summary statistics for each, and returns the result in a convenient form
print(aggr)


##### multiple plots #####

boxplot(sepal_length ~ species, data=iris, xlab="Species", ylab="Sepal Length")
plot(iris$sepal_length, iris$sepal_width, col=iris$species, pch=as.numeric(iris$species)) #col: color ; pch: marker type
#with(iris, plot(sepal_length, sepal_width, col=species, pch=as.numeric(species))) #alternative
plot(jitter(iris$sepal_length), jitter(iris$sepal_width))   #add noise 
smoothScatter(iris$sepal_length, iris$sepal_width)
pairs(iris)

library(scatterplot3d)
scatterplot3d(iris$petal_width, iris$sepal_length, iris$sepal_width)

distMatrix <- as.matrix(dist(iris[,1:4])) #Clustering methods classify data samples into groups of similar objects, here: similarity between different flowers in the iris data with dist()
heatmap(distMatrix)

library(lattice)
levelplot(petal_width~sepal_length*sepal_width, iris, cuts=9,col.regions=grey.colors(10)[10:1])

filled.contour(volcano, color=terrain.colors, asp=1,plot.axes=contour(volcano, add=T))
persp(volcano, theta=25, phi=30, expand=0.5, col="lightblue")

library(MASS)
parcoord(iris[1:4], col=iris$species)

parallelplot(~iris[1:4] | species, data=iris)

library(ggplot2)
qplot(sepal_length, sepal_width, data=iris, facets=species ~.)

dev.off()